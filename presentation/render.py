import time
import gym
import roboschool

env = gym.make('RoboschoolReacher-v1')
env.reset()
step = 0

while step < 10000:
	obs, reward, done, info = env.step(env.action_space.sample())
	if done:
		env.reset()
	env.render()
	time.sleep(0.0001)
	step += 1
env.close()